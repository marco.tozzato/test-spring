package it.infocamere.test.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "it.infocamere.test.web.controller" })
public class ConfigurationApp {

}
