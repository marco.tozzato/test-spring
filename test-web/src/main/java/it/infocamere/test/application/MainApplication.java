package it.infocamere.test.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration	
public class MainApplication {

	public MainApplication() {
		super();
	}
	
	public static void main (String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
}
