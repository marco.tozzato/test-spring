package it.infocamere.test.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.infocamere.test.web.objects.SimpleMessageReponse;

@RestController
public class SimpleController {

	@Autowired
	ApplicationContext ctx;
	@GetMapping("/helloworld")
	public @ResponseBody ResponseEntity<SimpleMessageReponse> simpleMessage(HttpServletRequest request,
			HttpServletResponse resrespose) {
		SimpleMessageReponse message=ctx.getBean(SimpleMessageReponse.class);
		try {
			message.setMessageBody("Hello world");
			
			return new ResponseEntity<SimpleMessageReponse>(message,HttpStatus.OK);
		}
		catch (Exception exc) {
			message.setMessageBody("Message error!!");
			return new ResponseEntity<SimpleMessageReponse>(message,HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}
}
